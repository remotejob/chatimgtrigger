package domains

import "time"

type Image struct {
	Id            int
	Name          string
	Age           int
	City          string
	Phone         string
	Img_file_name string
	SendDate      time.Time `json:"sendDate"`
	Type          string    `json:"type"`
	Message string  `json:"message"`
}
type ImageToSend struct {
	Id            string
	Name          string
	Age           string
	City          string
	Phone         string
	Img_file_name string
	SendDate      time.Time `json:"sendDate"`
}
type Joinmsg struct {
	// Age           int
	// City          string
	// Id            int
	// Img_file_name string
	// Name          string
	// Phone         string
	// Message       string `json:"message"`
	SendDate string `json:"sendDate"`
	Type     string `json:"type"`
}
