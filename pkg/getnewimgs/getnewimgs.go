package getnewimgs

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/remotejob/chatimgtrigger/internal/domains"
)

func Get(url string, imgnum string) ([]domains.Image, error) {

	if imgnum == "" {
		imgnum = "10"
	}

	var images []domains.Image

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("x-imgnum", imgnum)

	client := &http.Client{}
	r, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer r.Body.Close()

	json.NewDecoder(r.Body).Decode(&images)

	return images, nil

}
