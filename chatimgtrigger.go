package chatimgtrigger

import (
	"context"
	"time"

	"log"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/db"
	"gitlab.com/remotejob/chatimgtrigger/pkg/getnewimgs"
)


// FirestoreEvent is the payload of a Firestore event.
type FirestoreEvent struct {
	OldValue   FirestoreValue `json:"oldValue"`
	Value      FirestoreValue `json:"value"`
	UpdateMask struct {
		FieldPaths []string `json:"fieldPaths"`
	} `json:"updateMask"`
}

// FirestoreValue holds Firestore fields.
type FirestoreValue struct {
	CreateTime time.Time `json:"createTime"`
	// Fields is the data for this value. The type depends on the format of your
	// database. Log an interface{} value and inspect the result to see a JSON
	// representation of your database fields.
	Fields     Cliid     `json:"fields"`
	Name       string    `json:"name"`
	UpdateTime time.Time `json:"updateTime"`
}

type Cliid struct {
	Nguid struct {
		Value string `json:"stringValue"`
	} `json:"nguid"`
}


var client *db.Client

func init() {
	ctx := context.Background()

	conf := &firebase.Config{
		DatabaseURL: "https://fir-vuechatv6.firebaseio.com/",
	}
	app, err := firebase.NewApp(ctx, conf)
	if err != nil {
		log.Fatalf("firebase.NewApp: %v", err)
	}

	client, err = app.Database(ctx)
	if err != nil {
		log.Fatalf("app.Firestore: %v", err)
	}
}

func Clnewimg(ctx context.Context, e FirestoreEvent) error {
	guuid := e.Value.Fields
	newimgs, err := getnewimgs.Get("http://104.131.122.192:8001/api", "1")
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(newimgs)

	now := time.Now()

	ref := client.NewRef("room").Child(guuid.Nguid.Value + "/chats")

	for _, img := range newimgs {
		img.SendDate = now
		img.Message = img.Name + " Join chat"
		img.Type = "join"
		ref.Push(ctx, img)

	}

	ref = client.NewRef("imgs")
	var res map[string]interface{}
	ref.Get(ctx, &res)
	var resslice []string
	for k, _ := range res {
		resslice = append(resslice, k)

	}

	if len(resslice) > 5 {
		refn := ref.Child(resslice[0])
		refn.Delete(ctx)
		refn = ref.Child(resslice[1])
		refn.Delete(ctx)
	} else {
		refn := ref.Child(resslice[0])
		refn.Delete(ctx)

	}

	for _, img := range newimgs {
		img.SendDate = now
		ref.Push(ctx, img)

	}

	ref = client.NewRef("newimg").Child("last")
	newimgs[0].SendDate = now
	ref.Set(ctx,newimgs[0])


	return nil

}

